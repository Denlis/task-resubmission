var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var MyContractJSON = require(path.join(__dirname, 'build/contracts/student.json'))
var Web3 = require("web3");
const web3 = new Web3('http://localhost:8545');
accountAddress = "0xBc50C96D8aEE53b7Ebb8Dd6F5Fb9d00C989AbDc1";
const contractAddress = MyContractJSON.networks['4002'].address;
const contractAbi = MyContractJSON.abi;
MyContract = new web3.eth.Contract(contractAbi, contractAddress);
console.log(contractAddress);
var MyContractJSON = require(path.join(__dirname, 'build/contracts/student.json'))
// web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

// //contractAddress = MyContractJSON.networks['4002'].address;

// console.log(contractAddress);

// // get abi
// const abi = MyContractJSON.abi;

// // creating contract object
// MyContract = new web3.eth.Contract(abi, contractAddress);

// web3.eth.getAccounts().then((data) => {
//     console.log(data);;
// });


// coinbase = "0xBc50C96D8aEE53b7Ebb8Dd6F5Fb9d00C989AbDc1";
// console.log(coinbase);

web3.eth.getAccounts().then((data1)=> {
  console.log(data1);
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
