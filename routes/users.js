var express = require('express');
var router = express.Router();
var app  = require('../app');

var path = require('path');
var MyContractJSON = require(path.join(__dirname, '../build/contracts/student.json'))
var Web3 = require("web3");
const web3 = new Web3('http://localhost:8545');
accountAddress = "0xBc50C96D8aEE53b7Ebb8Dd6F5Fb9d00C989AbDc1";
const contractAddress = MyContractJSON.networks['4002'].address;
const contractAbi = MyContractJSON.abi;
MyContract = new web3.eth.Contract(contractAbi, contractAddress);
console.log(contractAddress);

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/home', function (req, res, next) {
  res.render('addDetails', { title: 'Add Details' });

});


var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";


  


router.post('/add', function (req, res, next) {
  //sres.render('addDetails', { title: 'Add Details' });
  
  


  data = req.body;
  console.log("data", data);
  console.log(web3);

  web3.eth.getAccounts().then((data1) => {
   

    MyContract.methods.setst_info(data.name, data.grade)
        .send({ from: data1[0], gas: 6000000 })
        .on('receipt', function (receipt) {
            console.log(receipt);

        }).on('error', (error) => {
            console.log(error.message);
            
        })
})

  MongoClient.connect(url,{ useUnifiedTopology: true }, function(err, db) {
      if (err) throw err;
      console.log("Database created!");
      var dbo = db.db("mydb");

      dbo.collection("students").drop(function(err, delOK) {
      if (err) throw err;
      if (delOK) console.log("Collection deleted");
      
      dbo.createCollection("students", function(err, res) {
      if (err) throw err;
      console.log("Collection created!");

      var myobj = { Name: data.name, Grade: data.grade };

      dbo.collection("students").insertOne(myobj, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();



    
      });});});

      // MyContract.methods.setst_info(1, data.name, data.grade)
      // .send({ from: data1[0], gas: 6000000 })
      // .on('receipt', function (receipt) {
      //     console.log(receipt);});

    // web3.eth.getAccounts().then((data1) => {
    //   db = new sqlite3.Database('./db/orderdb.db');

    //   student.methods.setst_info(data.id, data.name, data.grade)
    //   .send({ from: data1[1], gas: 6000000 })
    //   .on('receipt', function (receipt) {
    //     console.log(receipt);
    //     db.run('UPDATE orderDet set o_value = "Yes" where id = 1');
    //     res.render('i_InvoiceSuccess');
    //   }) .on('error', (error) => {
    //      console.log(error.message);
    //      res.render('i_InvoiceUnSucc');
    //    })
    // });
  });

  res.render('addDetails', { title: 'Add Details' });
});

router.post('/view', function (req, res, next) {

  // MongoClient.connect(url, function(err, db) {
  //   if (err) throw err;
  //   var dbo = db.db("mydb");
  //   dbo.collection("customers").find({}).toArray(function(err, result) {
  //     if (err) throw err;
  //     console.log(result);
  //     db.close();
  //   });
  // });
  web3.eth.getAccounts().then((data1) => {

  MyContract.methods.getst_info()
        .send({ from: data1[0] })
        .on('receipt', function (receipt) {
            console.log(receipt);

        }).on('error', (error) => {
            console.log(error.message);
            
        })})

  MongoClient.connect(url,{ useUnifiedTopology: true }, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    dbo.collection("students").find({}).toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
      
      res.render('viewDetails', { title: 'View Details' ,Data: result  });
      db.close();
    });
  });

  
});

router.post('/delete', function (req, res, next) {
  web3.eth.getAccounts().then((data1) => {
  MyContract.methods.delete_student()
        .send({ from: data1[0], gas: 6000000 })
        .on('receipt', function (receipt) {
            console.log(receipt);

        }).on('error', (error) => {
            console.log(error.message);
            
        })});



  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    // var myquery = { address: 'Mountain 21' };
    dbo.collection("students").deleteOne( function(err, obj) {
      if (err) throw err;
      console.log("1 document deleted");
      // db.close();
      dbo.collection("students").find({}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        
        res.render('viewDetails', { title: 'View Details' ,Data: result  });
        db.close();
    });})
  })})
module.exports = router;
