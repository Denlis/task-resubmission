// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;



contract student {

  struct st_info{
    
    string s_name;
    uint grade;
  }

  mapping(uint => st_info) public st_infomapping;

  function setst_info( string memory _name, uint _grade) public{
    st_infomapping[0]=st_info(_name,_grade);
  }

  function getst_info() public view returns(string memory _name, uint _grade) {
    require  (st_infomapping[0].grade!=0, "ENTRY DELETED");
    _name = st_infomapping[0].s_name;
    _grade = st_infomapping[0].grade;
  }
  
  function delete_student() public{
      
      delete st_infomapping[0];
  }

 
  }
