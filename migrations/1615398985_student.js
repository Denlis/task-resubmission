const { default: Web3 } = require("web3");


const student=artifacts.require("student");
module.exports = function(_deployer) {
  // Use deployer to state migration tasks.
  web3.eth.getAccounts().then((data1)=>{
    _deployer.deploy(student,data1[0]);
  })
};
